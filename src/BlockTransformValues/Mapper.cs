﻿using Modding.Blocks;
using ObjectExplorer.Mappings;
using System.Collections.Generic;
using UnityEngine;

namespace BlockScalingTools {

    public class Mapper : MonoBehaviour {

        void Awake() {
            ObjectExplorer.ObjectExplorer.AddMappings("BlockScalingTools",
                new MVector3<BraceCode>("StartRotation",
                    c => c.startPoint.localEulerAngles,
                    (c, x) => BraceWarping.Warp(c, Quaternion.Euler(x), c.endPoint.localRotation)
                ),
                new MVector3<BraceCode>("EndRotation",
                    c => c.endPoint.localEulerAngles,
                    (c, x) => BraceWarping.Warp(c, c.startPoint.localRotation, Quaternion.Euler(x))
                )
            );

            Destroy(this);
        }
    }

}
