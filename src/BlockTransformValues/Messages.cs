﻿using System;
using Modding;

/**
 * Scaling actions aren't shared over the network.
 */
namespace BlockScalingTools {
    // Token: 0x02000002 RID: 2
    public static class Messages {
        // Token: 0x04000001 RID: 1
        public static MessageType Scale;
        public static MessageType BraceWarp;
        public static MessageType UpdateSurface;
    }
}
