using Modding;
using Modding.Blocks;
using Modding.Mapper;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BlockScalingTools {
    public class Mod : ModEntryPoint {

        public static GameObject ModControllerObject;

        public static ModKey ScaleUniformKey;
        public static ModKey ScaleAsKey;

        public override void OnLoad() {
            CustomMapperTypes.AddMapperType<string, MTransform, TransformSelector>();

            // set up scale networking
            Messages.Scale = ModNetworking.CreateMessageType(new DataType[] {
                DataType.Block,
                DataType.Vector3,
                DataType.Boolean // BTM operation
            });
            ModNetworking.Callbacks[Messages.Scale] += msg => {
                Block block = (Block)msg.GetData(0);
                Vector3 newScale = (Vector3)msg.GetData(1);
                if (!(block == null || block.InternalObject == null)) {

                    if ((bool)msg.GetData(2)) {
                        TransformSelector.ScaleBlock(block.InternalObject, newScale);
                    }
                    else {
                        block.InternalObject.SetScale(newScale);
                    }

                }
            };

            // set up brace networking
            Messages.BraceWarp = ModNetworking.CreateMessageType(new DataType[] {
                DataType.Block,
                DataType.Vector3,
                DataType.Vector3
            });
            ModNetworking.Callbacks[Messages.BraceWarp] += msg => {
                BraceCode brace = ((Block)msg.GetData(0)).InternalObject as BraceCode;
                BraceWarping.WarpLocal(brace, Quaternion.Euler((Vector3)msg.GetData(1)), Quaternion.Euler((Vector3)msg.GetData(2)));
            };

            // set up surface networking
            Messages.UpdateSurface = ModNetworking.CreateMessageType(new DataType[] { DataType.Block });
            ModNetworking.Callbacks[Messages.UpdateSurface] += msg => {
                (((Block)msg.GetData(0)).InternalObject as BuildSurface)?.UpdateSurface();
            };

            // events
            Events.OnBlockInit += x => x.InternalObject.AddCustom(new MTransform(x.InternalObject));
            SceneManager.activeSceneChanged += (x, y) => OnSceneChanged();
            if (SceneManager.GetActiveScene().buildIndex == 9) {
                OnSceneChanged();
            }

            // resources
            ModResource.OnAllResourcesLoaded += () => {
                ValueHolderDrag.CursorTexture = ModResource.GetAssetBundle("cursor").LoadAsset<Texture2D>("cursor64");
            };

            // keys
            Mod.ScaleAsKey = ModKeys.GetKey("ScaleAs");
            Mod.ScaleUniformKey = ModKeys.GetKey("ScaleUniform");
            TransformSelector.CopyPositionKey = ModKeys.GetKey("CopyPosition");
            TransformSelector.CopyRotationKey = ModKeys.GetKey("CopyRotation");
            TransformSelector.CopyScaleKey = ModKeys.GetKey("CopyScale");
            TransformSelector.CopyAllKey = ModKeys.GetKey("CopyAll");
            TransformSelector.PasteKey = ModKeys.GetKey("Paste");
            ScaleButton.toggleKey = ModKeys.GetKey("scale-gizmo");

            // surfaces
            BuildSurface.AllowThicknessChange = true;
            BuildSurface.ShowCollisionToggle = true;
            BuildSurface.ShowMassSlider = true;
            Events.OnBlockInit += SurfaceNodeScaling;
            Events.OnBlockInit += HideOccluders;
            Events.OnBlockInit += x => x.GameObject?.transform.FindChild("DirectionArrow")?.gameObject.AddComponent<DirectionArrow>();

            // ModControllerObjects
            ModControllerObject = GameObject.Find("ModControllerObject");
            if (!ModControllerObject) UnityEngine.Object.DontDestroyOnLoad(ModControllerObject = new GameObject("ModControllerObject"));
            ModControllerObject.AddComponent<BraceWarping>();
            ModControllerObject.AddComponent<BlockScaleToolController>();
            ModControllerObject.AddComponent<MoreKeys>();
            ModControllerObject.AddComponent<UIFactory>();
            if (Mods.IsModLoaded(new Guid("3c1fa3de-ec74-44e4-807c-9eced79ddd3f"))) {
                ModControllerObject.AddComponent<Mapper>();
            }
        }

        private void HideOccluders(Block obj) {
            obj.InternalObject.transform.FindChild("Occluder")?.gameObject.SetActive(false);
            obj.InternalObject.transform.FindChild("occluder")?.gameObject.SetActive(false);
            obj.InternalObject.transform.FindChild("placement occluders")?.gameObject.SetActive(false);
        }
        public static bool SceneNotPlayable() {
            return SceneManager.GetActiveScene().buildIndex < 6 || SceneManager.GetActiveScene().buildIndex == 65;
        }

        private void OnSceneChanged() {
            if (Mod.SceneNotPlayable())
                return;

            // enable scaling block
            GameObject scalingBlock = GameObject.Find("HUD")?.transform.FindChild("BottomBar/Align (Bottom Left)/BLOCK BUTTONS/t_BASIC/Scaling Block (1)")?.gameObject;
            if (scalingBlock) {
                scalingBlock.SetActive(true);
                scalingBlock.transform.localPosition = new Vector3(5.275f, scalingBlock.transform.localPosition.y, scalingBlock.transform.localPosition.z);
            }

            // force enable intersections
            AllowIntersectionButton[] a = Resources.FindObjectsOfTypeAll<AllowIntersectionButton>();
            if (a.Length > 0)
                a[0].DisableIntersectionBlock();

            // setup gizmo offset display
            new GameObject("GizmoOffsetDisplay").AddComponent<GizmoOffsetDisplay>();

            // level editor tweaking
            if (SceneManager.GetActiveScene().name == "MasterSceneMultiplayer") {

                // scrollable level editor grid editing 
                LevelEditorUI ui = GameObject.Find("HUD").transform.FindChild("Snap left/LevelEditor").GetComponent<LevelEditorUI>();

                ScrollingValue scroll = ui.divisionsFields.Position.gameObject.AddComponent<ScrollingValue>();
                scroll.step = 0.1f;
                scroll.keptSteps = new float[] { 0.01f, 0.05f, 0.1f, 0.2f, 0.25f, 0.3f, 0.7f, 0.75f, 0.8f };

                scroll = ui.divisionsFields.Rotation.gameObject.AddComponent<ScrollingValue>();
                scroll.step = 5f;
                scroll.keptSteps = new float[] { 0.1f, 0.5f, 1f, 2f, 2.5f, 5f, 5.625f, 7.5f, 10f, 11.25f, 15f, 20f, 22.5f, 25f };

                scroll = ui.divisionsFields.Scale.gameObject.AddComponent<ScrollingValue>();
                scroll.step = 0.1f;
                scroll.keptSteps = new float[] { 0.005f, 0.01f, 0.05f, 0.1f, 0.2f, 0.25f, 0.3f, 0.7f, 0.75f, 0.8f };

                // force enable level editor grid editing ui
                if (!LevelEditorUI.DivisionFields.editingGrid && !StatMaster.isClient) {
                    try {
                        ui.options.ToggleDivisionsField();
                    }
                    catch { // smash
                    }
                }

                // local snap translate tools
                Transform translateTool = GameObject.Find("LevelEditorTools").transform.FindChild("EntityTransformParent/TranslateTool");
                foreach (Transform t in translateTool) {
                    EntityTranslateTool oldTool = t.GetComponent<EntityTranslateTool>();
                    if (!oldTool) {
                        continue;
                    }
                    t.gameObject.SetActive(false);

                    EntityTranslateToolLocalSnap newTool = t.gameObject.AddComponent<EntityTranslateToolLocalSnap>();
                    newTool.moveAcrossNormal = oldTool.moveAcrossNormal;
                    newTool.myRenderers = oldTool.myRenderers;
                    newTool.highlightMaterial = oldTool.highlightMaterial;
                    newTool.selectedMaterial = oldTool.selectedMaterial;
                    newTool.objToMove = translateTool.parent;
                    newTool.reverseTransforms = oldTool.reverseTransforms;

                    UnityEngine.Object.DestroyImmediate(oldTool);

                    t.gameObject.SetActive(true);
                }

                // hide gizmos while selecting
                GameObject.Find("LevelEditorTools").AddComponent<LevelEditorGizmoHide>();
            }
        }

        private void SurfaceNodeScaling(Block b) {
            if (b.Prefab.InternalObject.ID == (int)BlockType.BuildNode) {
                b.InternalObject.GetComponentInChildren<ScaleRelativeToCameraMinMax>().min = 0.05f;
            }
            else if (b.Prefab.InternalObject.ID == (int)BlockType.BuildEdge) {
                ScaleRelativeToCameraMinMax distScaleLine = b.InternalObject.transform.FindChild("VisLine").gameObject.AddComponent<ScaleRelativeToCameraMinMax>();
                distScaleLine.max = 1f;
                distScaleLine.min = 0.1f;
                distScaleLine.objectScale = 0.07f;

                ScaleRelativeToCameraMinMax distScaleHandle = b.InternalObject.transform.FindChild("VisHandle").gameObject.AddComponent<ScaleRelativeToCameraMinMax>();
                distScaleHandle.max = 1f;
                distScaleHandle.min = 0.1f;
                distScaleHandle.objectScale = 0.07f;
            }
        }

    }
}
