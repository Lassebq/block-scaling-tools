﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Modding;
using Modding.Blocks;

namespace BlockScalingTools {
    public class BlockScaleTool : BlockTransformTool {

        public enum ScaleAxis { X, Y, Z, All }

        public Transform objToMove;
        public override Transform Gizmo() {
            return objToMove;
        }

        public static float SnapValue = 0.05f;
        public ScaleAxis scaleAxis;

        public BlockScaleTool[] scaleTools;
        static Transform _scaleParent;
        public static Transform ScaleParent {
            get {
                return _scaleParent ? _scaleParent : (_scaleParent = new GameObject("BlockScaleParent").transform);
            }
        }

        public Transform toolTransform;

        public Material DefaultMaterial { get { return startMat; } set { startMat = value; } }

        private float basePosY = 2;
        private float baseReverseY = -2;

        private bool scalingMachine = false;
        private float currentScale = 1;

        private int lastIndex = 0;
        private bool changedThisFrame = true;

        void Start() {
            OnEnable();
        }

        protected override void ResetTool() {
            StatMaster.Mode.isScaling = false;
            if (scaleAxis == ScaleAxis.All) {
                foreach (BlockScaleTool entityScaleTool in scaleTools) {
                    entityScaleTool.UpdateTool(!reverse ? 1f : 0.5f);
                }
            }
            else {
                UpdateTool(!reverse ? 1f : 0.5f);
            }
            base.ResetTool();
        }

        public void ToolStateUpdate(BlockScaleBox.MouseState state) {
            switch (state) {
                case BlockScaleBox.MouseState.Enter:
                    OnMouseEnter();
                    break;
                case BlockScaleBox.MouseState.Exit:
                    OnMouseExit();
                    break;
                case BlockScaleBox.MouseState.Drag:
                    OnClickDrag();
                    break;
                case BlockScaleBox.MouseState.Down:
                    OnClicked();
                    break;
                case BlockScaleBox.MouseState.Up:
                    OnClickReleased();
                    break;
            }
        }

        protected override void UpdateReverse() {
            if (myRenderers.Length <= 1) {
                return;
            }
            if (!StatMaster.ToolActive && ReverseKey() != reverse) {
                reverse = !reverse;
                UpdateTool(!reverse ? 1f : 0.5f);
            }
        }

        protected override void OnGizmoDrag() {
            base.OnGizmoDrag();
            if (scaleAxis == ScaleAxis.All || reverse) {
                return;
            }
            GizmoPosition = movePosition + moveVector;
            float scale = GizmoLocalPosition.y / basePosY;
            UpdateTool(scale);
        }

        public void UpdateTool(float scale) {
            float distance = (!reverse ? basePosY : baseReverseY) * scale;
            GizmoLocalPosition = new Vector3(GizmoLocalPosition.x, distance, GizmoLocalPosition.z);

            Transform bar = myRenderers[0].transform;
            bar.localPosition = new Vector3(bar.localPosition.x, distance / 2f, bar.localPosition.z);
            bar.localScale = new Vector3(bar.localScale.x, distance, bar.localScale.z);

            currentScale = (scale - 1) * bar.parent.parent.parent.localScale.y * basePosY + 1f;
        }

        public override void OnClicked() {
            StatMaster.Mode.isScaling = true;
            StatMaster.Mode.currentBlockTool = this;
            ScaleParent.localScale = Vector3.one;
            scalingMachine = blockEditor.selectionController.Selection.Count == 0;
            GizmoClicked(scalingMachine
                ? Machine.Active().BuildingBlocks.Cast<ISelectable>().ToList()
                : blockEditor.selectionController.Selection);
            ScaleParent.position = toolTransform.position;
            ScaleParent.rotation = toolTransform.rotation;
        }

        protected override UndoAction CreateUndoAction(BlockBehaviour block, Vector3 oldPosition, Quaternion oldRotation, Vector3 oldScale) {
            if (block.Scale == oldScale && block.Position == oldPosition) {
                return null;
            }
            return new UndoActionScale(block.ParentMachine, block.Guid, block.Scale, oldScale,
                block.Position, oldPosition);
        }

        public float GetScaleValue(float scale) {
            scale = Mathf.Abs(scale);
            if (machineSelection.Count == 1) {
                return scale;
            }
            else {
                return scale >= OptionsMaster.minComponentUnit ? scale : OptionsMaster.minComponentUnit;
            }
        }

        protected override void TransformEntity(ISelectable entity, int index, bool useSnap) {
            BlockBehaviour block = entity as BlockBehaviour;
            if (!block)
                return;

            if (index > lastIndex && !changedThisFrame)
                return;

            Vector3 scaleMultiplier = Vector3.one;

            // calculate scale multiplier
            if (isStepping) {
                float actualSnap = 1f + (!ReverseKey() ? SnapValue : -SnapValue);

                if (scaleAxis == ScaleAxis.All || scalingMachine) {
                    scaleMultiplier *= actualSnap;
                }
                else {
                    scaleMultiplier[(int)scaleAxis] *= actualSnap;
                }
            }
            else {
                if (scaleAxis == ScaleAxis.All) {
                    Vector2 cursorPos = new Vector2(viewTanMoveVector.x, moveVector.y);
                    float distance = cursorPos.magnitude * Vector2.Dot(cursorPos.normalized, new Vector2(0.5f, 0.5f));
                    currentScale = 1f + distance;

                    scaleMultiplier *= useSnap ? Snap(currentScale, SnapValue) : currentScale;

                    foreach (BlockScaleTool entityScaleTool in scaleTools) {
                        entityScaleTool.UpdateTool(currentScale);
                    }
                }
                else if (scalingMachine) {
                    scaleMultiplier *= useSnap ? Snap(currentScale, SnapValue) : currentScale;
                }
                else {
                    scaleMultiplier[(int)scaleAxis] *= useSnap ? Snap(currentScale, SnapValue) : currentScale;
                }
            }

            scaleMultiplier.x = GetScaleValue(scaleMultiplier.x);
            scaleMultiplier.y = GetScaleValue(scaleMultiplier.y);
            scaleMultiplier.z = GetScaleValue(scaleMultiplier.z);

            // break if no change
            if (index <= lastIndex) {
                if (!(changedThisFrame = ScaleParent.localScale != scaleMultiplier)) {
                    return;
                }
                lastIndex = index;
            }

            // prepare transform for scaling
            Vector3 originalPosition = originalPositions[index];
            Vector3 originalScale = originalScales[index];
            block.transform.position = Machine.Active().BuildingMachine.TransformPoint(originalPosition);
            block.transform.localScale = originalScale;

            // handle unlinked and local modes
            if (!StatMaster.Mode.Transform.linked && !scalingMachine) {
                ScaleParent.position = StatMaster.Mode.Transform.pivot ? block.transform.position : block.GetCenter();
                if (!StatMaster.Mode.Transform.global) {
                    ScaleParent.rotation = block.transform.rotation;
                }
            }

            // do transform
            ScaleParent.localScale = Vector3.one;
            Transform parent = block.transform.parent;
            Vector3 pos = ScaleParent.position;
            block.transform.SetParent(ScaleParent, true);
            ScaleParent.localScale = scaleMultiplier;
            ScaleParent.localPosition = pos;
            block.transform.SetParent(parent, true);

            // finish transform
            block.SetPosition(block.transform.position);
            if (block.Prefab.ID == (int)BlockType.BuildNode || block.Prefab.ID == (int)BlockType.BuildEdge) {
                ScaleBlock(block, Vector3.one);
            }
            else {
                ScaleBlock(block, block.transform.localScale);
            }
        }

        public static void ScaleBlock(BlockBehaviour block, Vector3 scale) {
            block.SetScale(scale);
            ModNetworking.SendToAll(
                Messages.Scale.CreateMessage(
                    new object[] { Block.From(block), scale, false }));
        }

    }
}