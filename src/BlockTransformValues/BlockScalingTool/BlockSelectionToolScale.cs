﻿using UnityEngine;
using System.Collections;
using Modding;

namespace BlockScalingTools {
    public class BlockSelectionToolScale : BlockSelectionTool {

        public static void Consume(BlockSelectionTool bst) {
            BlockSelectionToolScale consumer = bst.gameObject.AddComponent<BlockSelectionToolScale>();
            AddPiece.Instance.selectionController = consumer;
            AdvancedBlockEditor.Instance.selectionController = consumer;
            consumer.selectionReference = bst.selectionReference;
            consumer.lastClickedTransformInfo = bst.lastClickedTransformInfo;
            bst.CopyProperties(consumer);
            consumer.ToolTransform = BlockScaleToolController.HandleInstance.transform;
            consumer.Init(AdvancedBlockEditor.Instance);
            Destroy(bst);
        }

        public override bool CanSelect() {
            Machine machine = Machine.Active();
            if (machine != null && StatMaster.advancedBuilding && !machine.isSimulating && !StatMaster.Mode.selectSymmetryPivot) {
                switch (StatMaster.Mode.selectedTool) {
                    case StatMaster.Tool.Translate:
                    case StatMaster.Tool.Rotate:
                    case StatMaster.Tool.Mirror:
                    case StatMaster.Tool.Scale:
                        return true;
                }
            }
            return false;
        }

    }
}