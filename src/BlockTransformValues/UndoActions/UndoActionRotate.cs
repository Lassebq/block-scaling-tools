﻿using UnityEngine;
using System;
using System.Collections;
using Modding;
using Modding.Blocks;
using System.Linq;

namespace BlockScalingTools {
    public class UndoActionRotate : UndoAction {

		private readonly Quaternion lastRot;
		private readonly Quaternion rot;

		public UndoActionRotate(Machine m, Guid blockGuid, Quaternion r, Quaternion lRot) {
			this.rot = r;
			this.lastRot = lRot;
			this.guid = blockGuid;
			this.changesTransform = true;
			this.machine = m;
		}

		public override bool Redo() {
			Transform buildingMachine = this.machine.BuildingMachine;
			this.machine.RotateBlock(this.guid, buildingMachine.rotation * this.rot);
			Update(false);
			return true;
		}

		public override bool Undo() {
			Transform buildingMachine = this.machine.BuildingMachine;
			this.machine.RotateBlock(this.guid, buildingMachine.rotation * this.lastRot);
			Update(true);
			return true;
		}

		public void Update(bool undo) {
			this.machine.GetBlock(guid, out BlockBehaviour block);
			if (!(block is BuildSurface)) return;
			(block as BuildSurface).UpdateSurface();
			(block as BuildSurface).nodes.ToList().ForEach(x => this.machine.RotateBlock(x.Guid, machine.BuildingMachine.rotation * ((undo) ? lastRot : rot)));
			ModNetworking.SendToAll(Messages.UpdateSurface.CreateMessage(new object[] { Block.From(guid) }));
		}
	}
}