﻿using System;
using UnityEngine;

namespace BlockScalingTools {
    public class EntityTranslateToolLocalSnap : EntityTranslateTool {

        public Transform objToMove;
        public override Transform Gizmo() {
            return objToMove;
        }

        protected override void TransformEntity(ISelectable entity, int index, bool useSnap) {
            if (entity == null) {
                return;
            }

            bool linked = StatMaster.Mode.LevelEditor.linked;
            bool global = StatMaster.Mode.LevelEditor.global;

            Vector3 offset = !linked && !global
                ? entity.GetTransform().TransformDirection(localMoveVector)
                : moveVector;
            if (useSnap) {
                offset = Snap(offset, !linked && !global ? entity.GetTransform() : Gizmo(), SNAP_VALUE);
            }

            entity.SetPosition(originalPositions[index] + offset);
        }
    }
}