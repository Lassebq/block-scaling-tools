﻿using Modding.Mapper;

namespace BlockScalingTools {

    /**
     * Mapper type thing.
     */
    public class MTransform : MCustom<string> {

        public BlockBehaviour Block;

        public MTransform(BlockBehaviour b) : base("Transform", "TransformHolder", "") {
            Block = b;
        }

        public override string DeSerializeValue(XData data) {
            return "";
        }

        public override XData SerializeValue(string value) {
            return new XString(SerializationKey, "");
        }
    }
}