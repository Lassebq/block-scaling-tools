﻿using Modding;
using Modding.Mapper;
using Selectors;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BlockScalingTools {
    public class ValueHolderDrag : MonoBehaviour {

        const float DEAD_ZONE = 0.05f;

        public static Texture2D CursorTexture;

        public float Value {
            get {
                Holder.TryGetValue(out float value);
                return value;
            }
            set {
                if (value == Value) {
                    return;
                }
                suppressEvents = true; // suppress OnValueChanged to avoid circular calls

                Holder.Terminate();
                Holder.SetValue(value);

                suppressEvents = false;
                if (!isDragging) {
                    InitialValue = value;
                }
            }
        }
        public float InitialValue { get; private set; }


        public Action<float> OnValueChangeStarted = null;
        public Action<float> OnValueDragged = null;
        public Action<float> OnValueChangeFinished = null;

        public bool Active { get { return Holder.IsSelected; } }
        public bool SelectedAll { get { return Holder.SelectedAll; } }

        bool _locked = false;
        public bool Locked {
            get { return _locked; }
            set {
                Holder.Lock(value);
                DragArea.enabled = !value;
                _locked = value;
            }
        }
        public float DragMultiplier = 1;

        private ValueHolder Holder;
        private UIButton DragArea;
        private MeshRenderer Renderer;

        private float mouseDragStart;
        private bool isDragging;
        private bool suppressEvents = false;

        private Color originalColor;

        public static ValueHolderDrag Create(Transform parent, Vector3 offset, string name, int localisationID, float initialValue,
            float dragMultiplier = 1, string suffix = "") {
            BMWidgetPool.Pool pool = BMWidgetPool.Instance.GetPool("Prefabs/BlockMapper/ValueContainer");
            GameObject mapper = pool.Get();
            pool.Add(mapper);

            GameObject root = Instantiate(mapper);
            root.name = name;
            root.transform.SetParent(parent, false);
            root.transform.localPosition = offset;
            root.transform.localScale = Vector3.one * 0.95f;
            DestroyImmediate(root.GetComponent<ContainerDetails>());
            DestroyImmediate(root.transform.FindChild("TextHolder").gameObject);
            DestroyImmediate(root.transform.FindChild("Background").gameObject);

            GameObject holder = root.transform.FindChild("ValueHolder").gameObject;
            holder.transform.FindChild("Background").localScale = new Vector3(1, 0.36f, 1);
            holder.transform.localPosition = Vector3.zero;
            DestroyImmediate(holder.GetComponent<Selectors.ValueSelector>());

            UIFactory.Text(root.transform, new Vector3(-0.605f, 0, 0), localisationID, 0.195f, layer: UIFactory.HUD_LAYER_KEYMAPPER,
                material: UIConfig.TEXT_MAT_STENCIL);

            ValueHolderDrag res = holder.AddComponent<ValueHolderDrag>();
            res.Value = initialValue;
            res.Holder.SetPrefixSuffix("", suffix);
            res.DragMultiplier = dragMultiplier;
            return res;
        }

        private void Awake() {
            Holder = GetComponent<ValueHolder>();
            Holder.ResetDelegate();
            Holder.CharLimit = 10;
            Holder.MaxDecimals = 3;
            Holder.ValueChanged += FinishValueChange;

            DragArea = GetComponent<UIButton>();
            DragArea.ResetDelegates();
            DragArea.Held += Dragging;
            DragArea.Down += Down;
            DragArea.MouseEnter += MouseEnter;
            DragArea.MouseExit += MouseExit;
            DragArea.Released += Released;

            Renderer = this.transform.FindChild("Background").GetComponent<MeshRenderer>();
            originalColor = Renderer.material.GetColor("_TintColor");
        }

        private void FinishValueChange(float value) {
            if (suppressEvents || isDragging) {
                return;
            }
            Debug.Log($"[BTM] [Value {this.transform.parent.parent.GetSiblingIndex()}-{this.transform.parent.GetSiblingIndex()}] ValueChangeFinished {value}");
            if (OnValueChangeFinished != null) {
                OnValueChangeFinished(value);
            }
        }

        private void Dragging() {
            float distance = (InputManager.CursorPosition().x - mouseDragStart);
            if (Mathf.Abs(distance) > DEAD_ZONE) {
                isDragging = true;

                float draggedValue = Mathf.Sign(distance) * Mathf.Pow(Mathf.Abs(distance) * 0.01f * DragMultiplier, 1.2f);

                Value = InitialValue + draggedValue;
                Debug.Log($"[BTM] [Value {this.transform.parent.parent.GetSiblingIndex()}-{this.transform.parent.GetSiblingIndex()}] Dragged {InitialValue + distance * 0.01f * DragMultiplier} {Value}");

                if (OnValueDragged != null) {
                    OnValueDragged(Value);
                }
            }

        }

        private void Down() {
            Debug.Log($"[BTM] [Value {this.transform.parent.parent.GetSiblingIndex()}-{this.transform.parent.GetSiblingIndex()}] ValuePressed {Value}");
            mouseDragStart = InputManager.CursorPosition().x;
            InitialValue = Value;

            if (OnValueChangeStarted != null) {
                OnValueChangeStarted(Value);
            }
        }

        private void Released() {
            Debug.Log($"[BTM] [Value {this.transform.parent.parent.GetSiblingIndex()}-{this.transform.parent.GetSiblingIndex()}] ValueReleased {Value}");
            if (!isDragging) {
                return;
            }
            isDragging = false;
            FinishValueChange(Value);
        }

        private void MouseEnter() {
            Cursor.SetCursor(CursorTexture, new Vector2(16f, 16f), CursorMode.Auto);
        }

        private void MouseExit() {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }

        /// <summary>
        /// Flash the background. For copy/paste events.
        /// </summary>
        public void Flash() {
            if (!Renderer) {
                return;
            }
            StopAllCoroutines();
            StartCoroutine(FlashAnimation());
        }

        protected IEnumerator FlashAnimation() {
            for (float t = 0f; t < 0.25f; t += Time.unscaledDeltaTime) {
                float i = Mathf.Lerp(1, originalColor.r, t / 0.25f);
                Renderer.material.SetColor("_TintColor", new Color(i, i, i, originalColor.a));
                yield return null;
            }
            yield break;
        }
    }
}
