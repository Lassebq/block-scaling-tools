﻿using System.Collections;
using UnityEngine;

namespace BlockScalingTools {
    internal class InputFieldPlaceholder : MonoBehaviour {

        public GameObject placeholder;

        void Start() {
            GetComponent<Selectors.TextHolder>().TextChanged += OnTextChanged;
        }

        private void OnTextChanged(string value) {
            placeholder.SetActive(value == null || value == "");
        }
    }
}