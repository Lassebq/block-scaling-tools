﻿using System;
using UnityEngine;

namespace BlockScalingTools {
    public class UIScaleOnMouseOver : MonoBehaviour {

        public void Awake() {
            if (!Target) {
                Target = this.transform;
            }

            try {
                Destroy(GetComponent<ScaleOnMouseOver>());
            }
            catch { }
        }

        private void OnMouseDown() {
            if (!base.enabled || !UIMask.InsideMask(this.mask, Target.position) || !this.active || !this.isMouseOver) {
                return;
            }
            Target.localScale = pressedScale;
        }

        private void OnMouseUp() {
            if (!base.enabled || !UIMask.InsideMask(this.mask, Target.position)) {
                this.OnMouseExit();
                return;
            }
            if (!this.active || !this.isMouseOver) {
                return;
            }
            Target.localScale = overScale;
        }

        private void OnMouseEnter() {
            if (!base.enabled || !UIMask.InsideMask(this.mask, Target.position)) {
                this.isMouseOver = false;
                return;
            }
            if (!this.active) {
                return;
            }
            this.isMouseOver = true;
            Target.localScale = overScale;
        }

        private void OnMouseExit() {
            if (!this.active) {
                return;
            }
            this.isMouseOver = false;
            Target.localScale = StartScale;
        }

        private void OnDisable() {
            this.isMouseOver = false;
            Target.localScale = this.StartScale;
        }

        private Transform _target = null;
        public Transform Target {
            get {
                return _target;
            }
            set {
                if (_target) {
                    OnDisable();
                }
                _target = value;
                StartScale = Target.localScale;
            }
        }

        public Vector3 StartScale;
        public float HoverScale = 1.15f;
        public float PressedScale = 0.85f;

        public float lerpSpeed = 0.1f;

        private Vector3 overScale { get { return StartScale * HoverScale; } }
        private Vector3 pressedScale { get { return StartScale * PressedScale; } }

        public int mask = -1;
        private bool isMouseOver;
        private bool active = true;
    }
}