[img=https://i.imgur.com/9MfO87r.png][/img]
                [b][url=https://besiege.fandom.com/wiki/Advanced_Building]Advanced Building Wiki Page[/url][/b]  |  [b][url=https://gitlab.com/dagriefaa/object-explorer]GitLab Repository[/url][/b]

[h1][b]                                         Uninstall EasyScale.[/b][/h1]

Modern Besiege revolves around two 'hidden' features - removing limits on value sliders, and changing the scale of blocks. Many machines rely on them because the game allows you to [b]load them without mods[/b].

However, you do need mods to [i]set[/i] them ingame.
[url=https://steamcommunity.com/sharedfiles/filedetails/?id=1473122210]NoBounds[/url] handles limits on sliders.
This mod handles scaling blocks. And more besides.

[url=https://steamcommunity.com/sharedfiles/filedetails/?id=1865492589]Colliderscope[/url] is recommended if you want to keep your sanity.

[h1]Scaling[/h1]
This is by far the most complete and properly-integrated scaling mod to ever be made for Besiege. Nothing - not EasyScale, not HardScale, not PBP - even comes close.
[list]
    [*]The [b]Scaling Gizmo[/b] allows fast and intuitive handling of multi-block selections
    [*]The [b]Transform Mapper[/b] allows precise tuning of individual blocks (with independent copy/paste and draggable infinite slider inputs)
    [*]A [b]numerical display for transform gizmos[/b] tells you precisely how much you're moving a selection, for fast and repeatable actions
    [*]Full support for Build Surfaces, making them responsive to scaling and preventing nasty edge cases
[/list]

[img=https://i.imgur.com/iywK7jn.png][/img]

[b]Build Surfaces[/b]
- Translation is read-only due to a crashing bug.
- Rotating in the gizmo only affects the hitboxes of the corner nodes. Use the rotate gizmo to rotate the whole surface.
- Scaling in the mapper does not change the positions of the nodes. Use the scale gizmo to make them move.

[img=https://i.imgur.com/S7TI7Ga.gif][/img]

[h1]Bracecube Creation/Warping[/h1]
[list]
    [*]Scale a brace to 0 on all axes and undo to make a bracecube.
    [*]Press Ctrl+D while rotating a brace to warp it.
[/list]

[url=https://besiege.fandom.com/wiki/Bracecube]Read the Bracecube wiki page for additional information.[/url]

[h1]Level Editor Tweaks[/h1]
This mod also brings the level editor tools closer to parity with the block editor tools by making these changes:
[list]
    [*]The translate/move tool snaps to the local orientation of the entity instead of locking it to a global grid.
    [*]The snap increments/grid size fields can be scrolled up and down, like the block snap fields.
    [*]The transform tools hide when holding LShift or when drag-selecting.
[/list]

[h1]Other Tweaks and Fixes[/h1]
[list]
    [*] Unhides smooth surface blocks/scaling blocks.
    [*] Unhides build surface mass and (visual) thickness sliders.
    [*] Enables placing intersecting blocks by default
    [*] Decreases the minimal size of surface nodes
    [*] Updates build surfaces when scaled
    [*] Makes rotation arrows (wheels, hinges, etc.) scale uniformly with the block, and hides them when the HUD is hidden
[/list]

[h1]Notes[/h1]
[list]
    [*] Rebind the shortcuts in the mod config files.
    [*] Mod is also known by: BST, BTM, ScalingTools™
    [*] Place bugs in comments with instructions on how to reproduce them.
[/list]

[h1]Known Bugs[/h1]
[list]
    [*] Translation in the transform mapper is disabled because it crashes servers.
[/list]
